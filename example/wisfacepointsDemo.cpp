// wisfacepointsDemo.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <windows.h>
#include <atlimage.h>
#include <opencv2/core/core.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "wisfacepoints.h"
#pragma  comment(lib, "wisfacepoints.lib")

int _tmain(int argc, _TCHAR* argv[])
{
	int detectEngine = CreateFacePointsEngine();
	cv::Mat img = cv::imread("1.jpg", 1);
	
	//转灰度图
	cv::Mat processedImage = img;
	RECT rtFaces[10];
	int facenum = DetectFaces(processedImage.data, processedImage.cols, processedImage.rows, processedImage.step.p[0], rtFaces);
	for(int i = 0; i < facenum; i++){
		cv::Rect rect (rtFaces[i].left,rtFaces[i].top, rtFaces[i].right-rtFaces[i].left, rtFaces[i].bottom - rtFaces[i].top);
		cv::rectangle(img, rect, cv::Scalar(0, 255, 255), 2);
		FacePointsModel fpm;
		float score = CalculateFacePoints(detectEngine, img.data, img.cols, img.rows, img.step.p[0], rtFaces[i], fpm);
		for (size_t i = 0; i < POINTS_NUM; i++)
		{
			cv::circle(img, cv::Point(fpm.points[i].x, fpm.points[i].y), 1, cv::Scalar(0, 255, 0), -1);
		}
		printf("roll=%f,yaw=%f,pitch=%f\n", fpm.roll, fpm.yaw, fpm.pitch);
		cv::imshow("face", img);
	}
	cv::waitKey(0);	
	return 0;
}

