package com.qs.facePose;

import com.qs.DefaultStruct;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Structure;


public class WisFacePose {

    public static final int POINTS_NUM = 51;

    public static class FacePointsModel extends DefaultStruct {
        public float roll;
        public float yaw;
        public float pitch;
        public POINT[] points = new POINT[POINTS_NUM];

        public static class ByValue extends FacePointsModel implements Structure.ByValue {
        }

        public static class ByReference extends FacePointsModel implements Structure.ByReference {
        }


        @Override
        public String toString() {
            return "roll :" + roll + " | yaw:" + yaw + " | pitch:" + pitch + points;
        }
    }

    public static class POINT extends DefaultStruct {
        public int x, y;

        public static class ByValue extends POINT implements Structure.ByValue {
        }


        @Override
        public String toString() {
            return "x:" + x + " | y" + y;
        }
    }

    public static class RECT extends DefaultStruct {
        public int left; // 矩形框左上角x坐标
        public int top; // 矩形框左上角y坐标
        public int right; // 矩形框右下角x坐标
        public int bottom; // 矩形框右下角y坐标

        public static class ByValue extends RECT implements Structure.ByValue {
        }


        public RECT.ByValue asValue() {
            ByValue val = new ByValue();
            val.left = this.left;
            val.top = this.top;
            val.right = this.right;
            val.bottom = this.bottom;
            return val;
        }
    }


    /**
     * 创建人脸特征点引擎engine
     */
    public static int createFacePointsEngine(String modelDir) {
        return WisFacePoseLibrary.INSTANCE.CreateFacePointsEngine(modelDir);
    }


    /**
     * @param engine 是CreateFacePointsEngine返回值
     * @param rgb24 照片的RGB数据区
     * @param width 照片的宽，
     * @param height 长
     * @param widthstep
     *            详细可参考http://www.opencv.org.cn/forum.php?mod=viewthread&tid=
     * @param rtFace 脸的矩形区域
     * @param fpm 人脸特征点以及三个角度的返回值
     * @return
     */
    public static float calculateFacePoints(int engine, byte[] rgb24, int width, int height, int widthstep,
            RECT.ByValue rtFace, FacePointsModel.ByReference fpm) {
        return WisFacePoseLibrary.INSTANCE.CalculateFacePoints(engine, rgb24, width, height, widthstep,
            rtFace, fpm);
    }


    /**
     * 快速人脸检测接口
     */
    public static int detectFaces(byte[] gray_data, int width, int height, int widthstep, RECT[] rtFaces) {
        return WisFacePoseLibrary.INSTANCE.DetectFaces(gray_data, width, height, widthstep, rtFaces);
    }

    public interface WisFacePoseLibrary extends Library {
        WisFacePoseLibrary INSTANCE =
                (WisFacePoseLibrary) Native.loadLibrary("wisfacepoints", WisFacePoseLibrary.class);


        int CreateFacePointsEngine(String modelDir);


        float CalculateFacePoints(int engine, byte[] rgb24, int width, int height, int widthstep,
                RECT.ByValue rtFace, FacePointsModel.ByReference fpm);


        int DetectFaces(byte[] gray_data, int width, int height, int widthstep, RECT[] rtFaces);

    }
}
