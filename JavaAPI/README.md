
### 使用
* 用GenCode.exe获取license
* 安装好程序，调用java接口即可使用
* 例子要使用不同的jdk运行版本。下面是详细


#### Facepose
> 检测人脸的pose，可以移动。目前提供32/64位版本

* 将dll目录文件加到当前工程根目录，或者其它能加载到dll文件的目录
* license文件保存在 JAVA_HOME\bin （32bit）目录下，即可执行文件java（.exe）同一目录
* 将dll目录下java_32_bin目录文件，放到执行本示例的32位java.exe对应目录(./bin)
* AgeGender API demo com.qs.ageGender.Test.testFasePose()

* dll目录文件,在JavaAPI统计目录的x86_32或者x86_64目录
* 64位版本同32位操作不过放到相应的64位执行环境下，注意切换不同的jdk
* 这里建议直接将x86_32或者x86_64目录下所有文件包括license放到java/bin目录下。