#include <stdio.h>
#include <Windows.h>
#define POINTS_NUM 51
typedef struct {
	float roll;
	float yaw;
	float pitch;
	POINT points[POINTS_NUM];
} FacePointsModel;

//创建人脸特征点引擎engine
int CreateFacePointsEngine(char *modelDir = NULL);

//engine 是CreateFacePointsEngine返回值
//rgb24 照片的RGB数据区
//width,height 照片的宽，长
//widthstep 详细可参考http://www.opencv.org.cn/forum.php?mod=viewthread&tid=1931
//rtFace 脸的举行区域
//fpm 人脸特征点以及三个角度的返回值
float CalculateFacePoints(int engine, unsigned char * rgb24, int width, int height, int widthstep, RECT rtFace, FacePointsModel &fpm);

//快速人脸检测接口
int DetectFaces(unsigned char * gray_data, int width, int height, int widthstep,RECT* rtFaces);